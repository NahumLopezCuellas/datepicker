// Recoger y pintar las fechas y dias de diferencia,
function showDatesAndDays() {

    // Recojo las fechas introducidas y las guardo en variables.
    dateIni = document.getElementById('ini').value;
    dateFin = document.getElementById('fin').value;

    // Pinto las fechas en los divs correspondientes.
    document.getElementById('containerIni').innerHTML = dateIni;
    document.getElementById('containerFin').innerHTML = dateFin;

    // Si ambas fechas tienen valor, pinto en otro div los dias de diferencia entre ellas.
    if (dateIni && dateFin) {
        document.getElementById('containerDays').innerHTML = calculateDays(dateIni, dateFin);
    }
}

// Calcular dias entre fechas. Recibe una fecha de inicio y otra de fin.
function calculateDays(dateIni, dateFin) {

    // Comprueba si las fechas tienen valor.
    if (dateIni && dateFin) {

        // Convierte las fechas (que vienen en formato String) a objetos Date (en milisegundos).
        var iniParse = Date.parse(dateIni);
        var finParse = Date.parse(dateFin);

        // Estas 3 variables sirven para convertir de milisegundos a dias.
        var minutes = 1000 * 60;
        var hours = minutes * 60;
        var days = hours * 24;

        // Calcula los dias entre las fechas y los devuelve.
        var daysBetweenDates = Math.round((finParse - iniParse) / days);
        return daysBetweenDates;
    }
}

// Cambia el formato de las fechas a String.
function changeFormatToDateString() {
    document.getElementById('containerIni').innerHTML = dateIni;
    document.getElementById('containerFin').innerHTML = dateFin;

    var dateIniObj = new Date(dateIni);
    var dateFinObj = new Date(dateFin);

    dateIniToDateString = dateIniObj.toDateString();
    dateFinToDateString = dateFinObj.toDateString();

    document.getElementById('containerIni').innerHTML = dateIniToDateString;
    document.getElementById('containerFin').innerHTML = dateFinToDateString;
}

// Cambia el formato de las fechas al formato local.
function changeFormatToLocaleDateString() {
    document.getElementById('containerIni').innerHTML = dateIni;
    document.getElementById('containerFin').innerHTML = dateFin;

    var dateIniObj = new Date(dateIni);
    var dateFinObj = new Date(dateFin);

    dateIniToLocaleDateString = dateIniObj.toLocaleDateString();
    dateFinToLocaleDateString = dateFinObj.toLocaleDateString();

    document.getElementById('containerIni').innerHTML = dateIniToLocaleDateString;
    document.getElementById('containerFin').innerHTML = dateFinToLocaleDateString;
}

// Limpiar campos.
function limpiar(){
  document.getElementById('ini').value = "";
  document.getElementById('fin').value = "";
  document.getElementById('containerIni').innerHTML = "";
  document.getElementById('containerFin').innerHTML = "";
  document.getElementById('containerDays').innerHTML = "";
}
