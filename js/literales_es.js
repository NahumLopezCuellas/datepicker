var i18n , i18n_es;
i18n = function(){
	"use strict";

	var vLiterales = {
		'litHeader': "Selector de fecha",
		'litIni': "Fecha de inicio:",
		'litFin': "Fecha de fin:",
		'litDias': "Días entre fechas:",
		'litFormatoString': "Cambiar formato de fecha a String",
		'litFormatoLocale': "Cambiar formato de fecha a Locale",
		'litES': "Español",
		'litEN': "English",
		'litLimpiar': "Limpiar"
	}
	return{
		literales: vLiterales
	};
};
i18n_es = new i18n();
