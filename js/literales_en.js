var i18n , i18n_en;
i18n = function(){
	"use strict";

	var vLiterales = {
		'litHeader': "Date",
		'litIni': "Start date:",
		'litFin': "End date:",
		'litDias': "Days between dates:",
		'litFormatoString': "Change format to String",
		'litFormatoLocale': "Change format to Locale",
		'litES': "Español",
		'litEN': "English",
		'litLimpiar': "Clear"
	}
	return{
		literales: vLiterales
	};
};
i18n_en = new i18n();
