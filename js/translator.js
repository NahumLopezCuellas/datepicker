var app = angular.module('myApp',['pascalprecht.translate']);
app.config(['$translateProvider', function ($translateProvider) {
  bloque_es = i18n_es.literales;
  bloque_en = i18n_en.literales;

  $translateProvider.translations('es',
    bloque_es
  );
  $translateProvider.translations('en',
    bloque_en
  );
  $translateProvider.preferredLanguage('es');
}]);

app.controller('MyController', ['$translate', '$scope', function ($translate, $scope) {

  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
  };

}])
